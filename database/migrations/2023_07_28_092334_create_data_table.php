<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data', function (Blueprint $table) {
            $table->id();
            $table->string('file_name')->unique();
            $table->string('material');
            $table->string('quantity');
            $table->string('delivery_id')->nullable();
            $table->string('document_id')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('hwb')->nullable();
            $table->string('contract_number')->nullable();
            $table->string('ponr_number')->nullable();
            $table->string('line_number')->nullable();
            $table->string('sonr')->nullable();
            $table->string('invoice_nr')->nullable();
            $table->string('despatch_date')->nullable();
            $table->string('arrival_date')->nullable();
            $table->string('inco_term')->nullable();
            $table->string('da_packages')->nullable();
            $table->string('pack_list_nr')->nullable();
            $table->string('transport_mode')->nullable();
            $table->string('carrier')->nullable();
            $table->string('van_id')->nullable();
            $table->string('mfr')->nullable();
            $table->string('interchange_control_reference')->nullable();
            $table->string('message_date')->nullable();
            $table->string('lig1')->nullable();
            $table->string('ctr1')->nullable();
            $table->string('fv1')->nullable();
            $table->string('lig2')->nullable();
            $table->string('ctr2')->nullable();
            $table->string('fv2')->nullable();
            $table->string('lig3')->nullable();
            $table->string('ctr3')->nullable();
            $table->string('fv3')->nullable();
            $table->string('supplier_material')->nullable();
            $table->string('weight')->nullable();
            $table->string('key')->nullable();
            $table->string('line')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data');
    }
};
