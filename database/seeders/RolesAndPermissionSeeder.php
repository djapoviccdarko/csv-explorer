<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRole = Role::create(['name' => 'admin']);

        Permission::create(['name' => 'users']);
        Permission::create(['name' => 'delete-users']);
        Permission::create(['name' => 'delete-data']);

        $adminRole->givePermissionTo([
            'users',
            'delete-users',
            'delete-data',
        ]);

        $user = User::create([
            'name' => 'admin',
            'surname' => 'admin',
            'email' => 'admin@admin.com',
            'title' => 'admin',
            'position' => 'admin',
            'password' => Hash::make('adminadmin'),
        ])->assignRole('admin');
    }
}
