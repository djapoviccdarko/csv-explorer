<?php

use Illuminate\Validation\Rules\Password;

return [
    'user_register' => [
        'validation_rules' => [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string'],
            'position' => ['required', 'string', 'alpha'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Password::min(8)
                ->letters()
                ->mixedCase()
                ->numbers()
                ->symbols()
                ->uncompromised()],

        ]
    ],
    'user_update' => [
        'validation_rules' => [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string'],
            'position' => ['required', 'string', 'alpha'],

        ]
    ],

];
