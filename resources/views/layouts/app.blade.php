<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description"
          content="{{__('layouts.description')}}"/>
    <meta name="author" content="Darko Djapovic"/>
    <meta name="keywords"
          content="desadv, brezna, avnet, csv data, desadv data, levelup, web app, data management,  data management">

    <!-- Page title -->
    <title>{{__('layouts.desadv_app')}}</title>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('images/AVT.svg') }}"/>

    <!-- Font Awesome icons (free version) -->
    <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
          type="text/css"/>

    <!-- Core theme CSS (includes Bootstrap) -->
    @vite(['./resources/css/style.css'])
</head>
<body id="page-top" @unless(Route::currentRouteName() === 'welcome') style="padding: 150px" @endunless>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
        <!-- Logo -->
        <a class="navbar-brand" href="{{ url('/') }}">{{__('layouts.desadv_app')}}</a>

        <!-- Toggle button for mobile menu -->
        <button class="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button"
                data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive"
                aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>

        <!-- Navigation links -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <!-- Display Dashboard link and user-specific dropdown when user is authenticated -->
            @if(Auth::user())
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item mx-0 mx-lg-1">
                        <a class="nav-link py-3 px-0 px-lg-3 rounded"
                           href="{{ route('home') }}">{{__('layouts.dashboard')}}</a>
                    </li>

                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item mx-0 mx-lg-1">
                            <a class="nav-link py-3 px-0 px-lg-3 rounded"
                               href="{{ route('insert') }}">{{__('layouts.upload')}}</a>
                        </li>
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item mx-0 mx-lg-1">
                                <a class="nav-link py-3 px-0 px-lg-3 rounded"
                                   href="{{ route('data') }}">{{__('layouts.desadv_data')}}</a>
                            </li>
                            <li class="nav-item mx-0 mx-lg-1 dropdown">
                                <a class="nav-link py-3 px-0 px-lg-3 rounded dropdown-toggle" href="#" role="button"
                                   data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>
                                <!-- User dropdown menu with logout option -->
                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('edit') }}">
                                        {{__('layouts.profile')}}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                        {{ __('layouts.logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                        @endif
                        <!-- Display login and register links when user is not authenticated -->
                        @guest()
                            <ul class="navbar-nav ms-auto">
                                @if (Route::has('login'))
                                    <li class="nav-item mx-0 mx-lg-1">
                                        <a class="nav-link py-3 px-0 px-lg-3 rounded"
                                           href="{{ route('login') }}">{{__('auth.login')}}</a>
                                    </li>
                                @endif
                                @if (Route::has('register'))
                                    <li class="nav-item mx-0 mx-lg-1">
                                        <a class="nav-link py-3 px-0 px-lg-3 rounded"
                                           href="{{ route('register') }}">{{__('auth.register')}}</a>
                                    </li>
                                @endif
                            </ul>
            @endguest
        </div>
    </div>
</nav>

<!-- Content Section (To be filled with page-specific content) -->
@yield('content')

<!-- Copyright Section -->
<div
    class="copyright py-4 text-center text-white @unless(Route::currentRouteName() === 'welcome') fixed-bottom @endunless">
    <div class="container"><small>Copyright &copy; LevelUp</small></div>
</div>

<!-- Bootstrap core JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>

<!-- Core theme JS (includes custom scripts) -->
@vite(['resources/js/scripts.js'])

<!-- Forms JS for Start Bootstrap (if used) -->
<script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>
</html>
