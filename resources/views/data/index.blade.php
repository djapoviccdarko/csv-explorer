@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <h2>{{__('data.desadv_data')}} </h2>
        <hr>
        @if(count($data) == 0)
            <div class="alert alert-danger">
                    {{__('data.no_results')}}
            </div>
        @else
            <p>{{__('data.details_message')}}</p>
            <form role="search" method="GET" action="{{route('search')}}" class="mb-2">
                <div class="input-group">
                    <input type="search" class="form-control"
                           placeholder="{{__('data.search_placeholder')}}" name="query">
                    <button type="submit" class="btn btn-secondary">
                        {{__('data.search')}}
                    </button>
                </div>
            </form>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>{{__('data.file_name')}}</th>
                    <th>{{__('data.supplier')}}</th>
                    <th>{{__('data.material')}}</th>
                    <th>{{__('data.ponr_number')}}</th>
                    <th>{{__('data.line_number')}}</th>
                    <th>{{__('data.quantity')}}</th>
                    <th>{{__('data.invoice_nr')}}</th>
                    <th>{{__('data.arrival_date')}}</th>

                </tr>
                </thead>
                <tbody>
                @foreach ($data as $csvData)
                    <tr onclick="window.location='{{ route('details', ['id' => $csvData['id']]) }}';"
                        style="cursor:pointer">
                        <td>{{ $csvData->file_name }}</td>
                        <td>{{ $csvData->mfr }}</td>
                        <td>{{ $csvData->material }}</td>
                        <td>{{ $csvData->ponr_number }}</td>
                        <td>{{ $csvData->line_number }}</td>
                        <td>{{ $csvData->quantity }}</td>
                        @if(!empty($csvData->invoice_nr))
                            <td>{{ $csvData->invoice_nr }}</td>
                        @else
                            <td> NULL</td>
                        @endif
                        <td>{{ $csvData->arrival_date }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination">
                {{ $data->links() }}
            </div>
    </div>
    @endif

    <div class="d-flex justify-content-center align-items-center mt-2">
        <button class="btn btn-secondary col-md-6"
                onclick="window.location='{{ route('home') }}'">{{__('data.back')}}
        </button>
    </div>
@endsection
