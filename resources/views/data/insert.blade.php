@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->has('csv_files') || $errors->has('csv_files.*'))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {!! $error !!}
                @endforeach
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
        @endif

        <h2>{{__('data.upload_files')}}</h2>
        <hr>
        <p>{{__('data.upload_details')}}</p>
        <form action="{{ route('upload') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input class="form-control" type="file" name="csv_files[]" multiple><br><br>
            <div class="d-flex justify-content-center align-items-center">
                <button class="btn btn-success col-md-6" type="submit">{{__('data.upload')}}</button>
            </div>
        </form>
        <div class="d-flex justify-content-center align-items-center mt-2">
            <button class="btn btn-secondary col-md-6"
                    onclick="window.location='{{ route('home') }}'">{{__('data.back')}}
            </button>
        </div>
    </div>

@endsection
