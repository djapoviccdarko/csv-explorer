@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-center">{{__('data.DESADV_FILE')}}: {{ $csvData->file_name }}</h2>
        <hr>
        <div class="row">
            <div class="col-md-4">
                @if(!empty($csvData->material))
                    <p>{{ __('data.material') }}: {{ $csvData->material }}</p>
                @else
                    <p>{{ __('data.material') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->quantity))
                    <p>{{ __('data.quantity') }}: {{ $csvData->quantity }}</p>
                @else
                    <p>{{ __('data.quantity') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->delivery_id))
                    <p>{{ __('data.delivery_id') }}: {{ $csvData->delivery_id }}</p>
                @else
                    <p>{{ __('data.delivery_id') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->document_id))
                    <p>{{ __('data.document_id') }}: {{ $csvData->document_id }}</p>
                @else
                    <p>{{ __('data.document_id') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->tracking_number))
                    <p>{{ __('data.tracking_number') }}: {{ $csvData->tracking_number }}</p>
                @else
                    <p>{{ __('data.tracking_number') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->hwb))
                    <p>{{ __('data.hwb') }}: {{ $csvData->hwb }}</p>
                @else
                    <p>{{ __('data.hwb') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->contract_number))
                    <p>{{ __('data.contract_number') }}: {{ $csvData->contract_number }}</p>
                @else
                    <p>{{ __('data.contract_number') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->ponr_number))
                    <p>{{ __('data.ponr_number') }}: {{ $csvData->ponr_number }}</p>
                @else
                    <p>{{ __('data.ponr_number') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->line_number))
                    <p>{{ __('data.line_number') }}: {{ $csvData->line_number }}</p>
                @else
                    <p>{{ __('data.line_number') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->sonr))
                    <p>{{ __('data.sonr') }}: {{ $csvData->sonr }}</p>
                @else
                    <p>{{ __('data.sonr') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->invoice_nr))
                    <p>{{ __('data.invoice_nr') }}: {{ $csvData->invoice_nr }}</p>
                @else
                    <p>{{ __('data.invoice_nr') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->despatch_date))
                    <p>{{ __('data.despatch_date') }}: {{ $csvData->despatch_date }}</p>
                @else
                    <p>{{ __('data.despatch_date') }}: {{ __('data.no_information') }}</p>
                @endif

            </div>
            <div class="col-md-4">

                @if(!empty($csvData->arrival_date))
                    <p>{{ __('data.arrival_date') }}: {{ $csvData->arrival_date }}</p>
                @else
                    <p>{{ __('data.arrival_date') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->inco_term))
                    <p>{{ __('data.inco_term') }}: {{ $csvData->inco_term }}</p>
                @else
                    <p>{{ __('data.inco_term') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->da_packages))
                    <p>{{ __('data.da_packages') }}: {{ $csvData->da_packages }}</p>
                @else
                    <p>{{ __('data.da_packages') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->pack_list_nr))
                    <p>{{ __('data.pack_list_nr') }}: {{ $csvData->pack_list_nr }}</p>
                @else
                    <p>{{ __('data.pack_list_nr') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->transport_mode))
                    <p>{{ __('data.transport_mode') }}: {{ $csvData->transport_mode }}</p>
                @else
                    <p>{{ __('data.transport_mode') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->carrier))
                    <p>{{ __('data.carrier') }}: {{ $csvData->carrier }}</p>
                @else
                    <p>{{ __('data.carrier') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->van_id))
                    <p>{{ __('data.van_id') }}: {{ $csvData->van_id }}</p>
                @else
                    <p>{{ __('data.van_id') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->mfr))
                    <p>{{ __('data.supplier') }}: {{ $csvData->mfr }}</p>
                @else
                    <p>{{ __('data.supplier') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->interchange_control_reference))
                    <p>{{ __('data.interchange_control_reference') }}: {{ $csvData->interchange_control_reference }}</p>
                @else
                    <p>{{ __('data.interchange_control_reference') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->message_date))
                    <p>{{ __('data.message_date') }}: {{ $csvData->message_date }}</p>
                @else
                    <p>{{ __('data.message_date') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->lig1))
                    <p>{{ __('data.lig1') }}: {{ $csvData->lig1 }}</p>
                @else
                    <p>{{ __('data.lig1') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->ctr1))
                    <p>{{ __('data.ctr1') }}: {{ $csvData->ctr1 }}</p>
                @else
                    <p>{{ __('data.ctr1') }}: {{ __('data.no_information') }}</p>
                @endif

            </div>
            <div class="col-md-4">

                @if(!empty($csvData->fv1))
                    <p>{{ __('data.fv1') }}: {{ $csvData->fv1 }}</p>
                @else
                    <p>{{ __('data.fv1') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->lig2))
                    <p>{{ __('data.lig2') }}: {{ $csvData->lig2 }}</p>
                @else
                    <p>{{ __('data.lig2') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->ctr2))
                    <p>{{ __('data.ctr2') }}: {{ $csvData->ctr2 }}</p>
                @else
                    <p>{{ __('data.ctr2') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->fv2))
                    <p>{{ __('data.fv2') }}: {{ $csvData->fv2 }}</p>
                @else
                    <p>{{ __('data.fv2') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->lig3))
                    <p>{{ __('data.lig3') }}: {{ $csvData->lig3 }}</p>
                @else
                    <p>{{ __('data.lig3') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->ctr3))
                    <p>{{ __('data.ctr3') }}: {{ $csvData->ctr3 }}</p>
                @else
                    <p>{{ __('data.ctr3') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->fv3))
                    <p>{{ __('data.fv3') }}: {{ $csvData->fv3 }}</p>
                @else
                    <p>{{ __('data.fv3') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->supplier_material))
                    <p>{{ __('data.supplier_material') }}: {{ $csvData->supplier_material }}</p>
                @else
                    <p>{{ __('data.supplier_material') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->weight))
                    <p>{{ __('data.weight') }}: {{ $csvData->weight }}</p>
                @else
                    <p>{{ __('data.weight') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->key))
                    <p>{{ __('data.key') }}: {{ $csvData->key }}</p>
                @else
                    <p>{{ __('data.key') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->line))
                    <p>{{ __('data.line') }}: {{ $csvData->line }}</p>
                @else
                    <p>{{ __('data.line') }}: {{ __('data.no_information') }}</p>
                @endif

                @if(!empty($csvData->file_name))
                    <p>{{ __('data.file_name') }}: {{ $csvData->file_name }}</p>
                @else
                    <p>{{ __('data.file_name') }}: {{ __('data.no_information') }}</p>
                @endif
            </div>
        </div>
        @can('delete-data')
            <div class="d-flex justify-content-center align-items-center mt-2">
                <a href="{{ route('remove', ['fileName' => $csvData->file_name]) }}"
                   onclick="return confirm('Are you sure you want to remove this file?')"
                   class="btn btn-danger col-md-6">
                    {{__('data.remove_desadv')}}
                </a>
            </div>
        @endcan

        <div class="d-flex justify-content-center align-items-center mt-2">
            <button class="btn btn-secondary col-md-6"
                    onclick="window.location='{{ route('data') }}'">{{__('data.back')}}</button>
        </div>
    </div>
@endsection

