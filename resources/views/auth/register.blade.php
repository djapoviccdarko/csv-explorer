@extends('layouts.app') <!-- Extending the 'layouts.app' Blade layout -->

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('auth.register') }}</div> <!-- Register Card Header -->
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}"> <!-- Register Form -->
                            @csrf <!-- CSRF Token -->

                            <!-- Name Field -->
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('auth.name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name') <!-- Displaying Name validation errors -->
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Surname Field -->
                            <div class="row mb-3">
                                <label for="surname"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.surname') }}</label>
                                <div class="col-md-6">
                                    <input id="surname" type="text"
                                           class="form-control @error('surname') is-invalid @enderror" name="surname"
                                           value="{{ old('surname') }}" required autocomplete="surname">
                                    @error('surname') <!-- Displaying Surname validation errors -->
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Title Field -->
                            <div class="row mb-3">
                                <label for="title"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.title') }}</label>
                                <div class="col-md-6">
                                    <input id="title" type="text"
                                           class="form-control @error('title') is-invalid @enderror" name="title"
                                           value="{{ old('title') }}" required autocomplete="title">
                                    @error('title') <!-- Displaying Title validation errors -->
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Position Field -->
                            <div class="row mb-3">
                                <label for="position"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.position') }}
                                    <i class="fas fa-info-circle ml-2" data-bs-toggle="tooltip" data-bs-placement="top"
                                       title="{{__('auth.position_details')}}"></i>
                                </label>
                                <div class="col-md-6">
                                    <input id="position" type="text"
                                           class="form-control @error('position') is-invalid @enderror" name="position"
                                           value="{{ old('position') }}" required autocomplete="position">
                                    @error('position') <!-- Displaying Title validation errors -->
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Email Address Field -->
                            <div class="row mb-3">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.email_address') }}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" required autocomplete="email">
                                    @error('email') <!-- Displaying Email validation errors -->
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Password Field -->
                            <div class="row mb-3">
                                <label for="password" class="col-md-4 col-form-label text-md-end">
                                    {{ __('auth.password') }}
                                    <!-- Info Icon -->
                                    <i class="fas fa-info-circle ml-2" data-bs-toggle="tooltip" data-bs-placement="top"
                                       title="{{ __('auth.password_requirements') }}"></i>
                                </label>
                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password"
                                           required
                                           autocomplete="new-password">

                                    <!-- Displaying Password validation errors -->
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Confirm Password Field -->
                            <div class="row mb-3">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.confirm_password') }}
                                </label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <!-- Register Button -->
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-secondary">
                                        {{ __('auth.register') }}
                                    </button>
                                    <!-- Already have an account? LogIn Link -->
                                    <a class="btn btn-link" href="{{ route('login') }}">
                                        {{ __('auth.account_exist') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
