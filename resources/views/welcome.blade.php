@extends('layouts.app')

@section('content')
    <header class="masthead bg-primary text-white text-center">
        <div class="container d-flex align-items-center flex-column">
            <img class="masthead-avatar mb-5" src="{{ asset('images/AVT.svg') }}" alt="Desadv Data Management Logo">
            <h1 class="masthead-heading text-uppercase mb-0">{{__('welcome.desadv')}}</h1>
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <p class="masthead-subheading font-weight-light mb-0">{{__('welcome.levelup')}}</p>
        </div>
    </header>

    <footer class="footer text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h4 class="text-uppercase mb-4">{{__('welcome.location')}}</h4>
                    <p class="lead mb-0">
                        Corporate Headquarters<br>
                        Avnet, Inc.<br>
                        2211 South 47th Street<br>
                        Phoenix, AZ USA 85034<br>
                        {{__('welcome.phone')}}: 480-643-2000
                    </p>
                </div>

                <div class="col-lg-4 mb-5 mb-lg-0">
                    <h4 class="text-uppercase mb-4">{{__('welcome.around_the_web')}}</h4>
                    <a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/AvnetInc/"><i
                            class="fab fa-fw fa-facebook-f"></i></a>
                    <a class="btn btn-outline-light btn-social mx-1" href="https://twitter.com/Avnet"><i
                            class="fab fa-fw fa-twitter"></i></a>
                    <a class="btn btn-outline-light btn-social mx-1" href="https://www.linkedin.com/company/avnet/"><i
                            class="fab fa-fw fa-linkedin-in"></i></a>
                    <a class="btn btn-outline-light btn-social mx-1" href="https://www.instagram.com/avnet/?hl=en"><i
                            class="fab fa-fw fa-instagram"></i></a>
                </div>

                <div class="col-lg-4">
                    <h4 class="text-uppercase mb-4">{{__('welcome.about')}}</h4>
                    <p class="lead mb-0">
                        <a href="https://www.avnet.com/wps/portal/us/">Avnet</a> {{__('welcome.about_text')}}
                    </p>
                </div>
            </div>
        </div>
    </footer>
@endsection
