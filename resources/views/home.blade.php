@extends('layouts.app')

@section('content')
    <div class="container">

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <h2>{{__('data.dashboard')}} </h2>
        <hr class="mb-5">
        @if(auth()->user()->hasRole('admin'))
            <div class="row">
                @else
                    <div class="row" style="margin-top:3rem">
                        @endif
                        <div class="col-md-6">
                            <a href="{{route('data')}}" style="text-decoration: none">
                                <div class="card mb-4">
                                    <div class="card-header">
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">{{__('dashboard.desadv_management')}}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted"></h6>
                                        <p class="card-text">{{__('dashboard.manage_desadv')}}</p>
                                    </div>
                                    <div class="card-footer">
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('insert') }}" style="text-decoration: none">
                                <div class="card mb-4">
                                    <div class="card-header">
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">{{__('dashboard.insert_data')}}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted"></h6>
                                        <p class="card-text">{{__('dashboard.insert_message')}}</p>
                                    </div>
                                    <div class="card-footer">
                                    </div>
                                </div>
                            </a>
                        </div>
                        @can('users')
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 justify-content-center m-auto">
                                        <a href="{{ route('users') }}" style="text-decoration: none">
                                            <div class="card w-100 mb-4">
                                                <div class="card-header">
                                                </div>
                                                <div class="card-body">
                                                    <h5 class="card-title">{{__('dashboard.users_management')}}</h5>
                                                    <h6 class="card-subtitle mb-2 text-muted"></h6>
                                                    <p class="card-text">{{__('dashboard.users_message')}}</p>
                                                </div>
                                                <div class="card-footer">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endcan
@endsection
