@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <table class="table align-middle mb-0 bg-white border-1">
            <thead class="bg-light">
            <tr>
                <th>{{__('auth.name')}}</th>
                <th>{{__('auth.title')}}</th>
                <th>{{__('auth.position')}}</th>
                <th>{{__('auth.remove_user')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="ms-3">
                                <p class="fw-bold mb-1">{{$user->name . ' ' .$user->surname}}</p>
                                <p class="text-muted mb-0">{{$user->email}}</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <p class="fw-normal mb-1">{{$user->title}}</p>
                    </td>
                    <td>{{$user->position}}</td>
                    @can('delete-users')
                        <td>
                            <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                @if($user->name !== 'admin')
                                    <button type="submit" class="btn btn-danger w-100  mt-1"
                                            onclick="return confirm('Are you sure you want delete this User')">{{__('auth.remove')}}
                                    </button>
                                @endif
                            </form>
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination mt-2">
            {{ $users->links() }}
        </div>
    </div>
    <div class="d-flex justify-content-center align-items-center mt-2">
        <button class="btn btn-secondary col-md-6"
                onclick="window.location='{{ route('home') }}'">{{__('data.back')}}</button>
    </div>
@endsection
