@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('auth.edit_profile') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('update') }}">
                            @csrf <!-- CSRF Token -->
                            @method('PUT')

                            <!-- Name Field -->
                            <div class="row mb-3">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') ?? $user->name}}" autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Surname Field -->
                            <div class="row mb-3">
                                <label for="surname"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.surname') }}</label>
                                <div class="col-md-6">
                                    <input id="surname" type="text"
                                           class="form-control @error('surname') is-invalid @enderror" name="surname"
                                           value="{{ old('surname') ?? $user->surname}}" autocomplete="surname"
                                           autofocus>
                                    @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Title Field -->
                            <div class="row mb-3">
                                <label for="title"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.title') }}</label>
                                <div class="col-md-6">
                                    <input id="title" type="text"
                                           class="form-control @error('title') is-invalid @enderror" name="title"
                                           value="{{ old('title') ?? $user->title}}" autocomplete="title"
                                           autofocus>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Position Field -->
                            <div class="row mb-3">
                                <label for="position"
                                       class="col-md-4 col-form-label text-md-end">{{ __('auth.position') }}
                                    <i class="fas fa-info-circle ml-2" data-bs-toggle="tooltip" data-bs-placement="top"
                                       title="{{__('auth.position_details')}}"></i>
                                </label>
                                <div class="col-md-6">
                                    <input id="position" type="text"
                                           class="form-control @error('position') is-invalid @enderror" name="position"
                                           value="{{ old('position') ?? $user->position}}" autocomplete="position"
                                           autofocus>
                                    @error('position')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- Register Button -->
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary justify-content-center"
                                            onclick="return confirm('{{__('auth.edit_message')}}')">
                                        {{__('auth.edit')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
