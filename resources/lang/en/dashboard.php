<?php
return [
    'desadv' => 'Desadv',
    'desadv_management' => 'DESADV management',
    'manage_desadv' => 'Explore and Manage DESADV Files Here.',
    'insert' => 'Insert',
    'insert_data' => 'Insert data',
    'insert_message' => 'Click here to insert new DESADV data.',
    'users' => 'Users',
    'users_management' => 'Users management',
    'users_message' => 'Click here to view or remove users.',

];

