<?php
return [
    'desadv' => 'DESADV',
    'levelup' => 'Level Up - Brezna - Avnet',
    'location' => 'Location',
    'phone' => 'Phone',
    'around_the_web' => 'Around the web',
    'about' => 'About',
    'about_text' => 'is a global leader of electronic
                        components and services, guiding makers and manufacturers
                        from design to delivery.'
];
