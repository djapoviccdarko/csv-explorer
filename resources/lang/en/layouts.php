<?php
return [
    'description' => 'Our application aims to provide users with a user-friendly interface to visualize and interact with inbound DESADV (Delivery Advice) data extracted from CSV files. The system comprises a robust backend responsible for consuming and manipulating data, as well as persisting it in various storage options such as files or databases. Additionally, we have an intuitive frontend that enables users to access and analyze this information effortlessly.',
    'desadv_app'=> 'DESADV application',
    'dashboard'=> 'Dashboard',
    'upload'=> 'Upload',
    'desadv_data'=> 'DESADV data',
    'profile'=> 'Profile',
    'logout'=> 'Logout',
];

