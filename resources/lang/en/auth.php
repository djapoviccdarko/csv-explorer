<?php
return [
    'password_requirements' => 'Your password must meet the following requirements:
                               - Minimum length of 8 characters
                               - At least one letter
                               - At least one upper case and lower case letter (mixed case)
                               - At least one number
                               - At least one symbol',
    'position_details' => 'Junior, Medior or Senior',
    'confirm' => 'Confirm Password',
    'confirm_message' => 'Please confirm your password before continuing.',
    'password' => 'Password',
    'forgot' => 'Forgot Your Password?',
    'reset' => 'Reset Password',
    'email_address' => 'Email Address',
    'reset_link' => 'Send Password Reset Link',
    'login' => 'Login',
    'remember' => 'Remember me',
    'register' => 'Register',
    'name' => 'Name',
    'surname' => 'Surname',
    'title' => 'Job Title',
    'position' => 'Position',
    'confirm_password' => 'Confirm Password',
    'account_exist' => 'Already have an account? Log In!',
    'verify' => 'Verify Your Email Address',
    'verification_link' => 'A fresh verification link has been sent to your email address.',
    'check_link' => 'Before proceeding, please check your email for a verification link.',
    'not-receive' => 'If you did not receive the email',
    'request_another' => 'click here to request another',
    'edit_message' => 'Are you sure you want to edit your profile?',
    'edit' => 'Edit',
    'edit_profile' => 'Edit profile',
    'remove_user' => 'Remove user',
    'remove' => 'Remove',


];
