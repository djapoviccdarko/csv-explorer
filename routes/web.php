<?php

use App\Http\Controllers\Data\DataController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Users\UserController;
use App\Models\Data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes([
    'verify' => true
]);

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/users', [UserController::class, 'index'])->name('users')->middleware('admin');
Route::get('/edit', [UserController::class, 'edit'])->name('edit');
Route::put('/update', [UserController::class, 'update'])->name('update');
Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('users.destroy');
Route::get('/data', [DataController::class, 'index'])->name('data');
Route::get('/search', [DataController::class, 'search'])->name('search');
Route::get('/insert', [DataController::class, 'insertView'])->name('insert');
Route::post('/upload', [DataController::class, 'upload'])->name('upload');
Route::get('/details/{id}', [DataController::class, 'showDetails'])->name('details');
Route::get('/remove/{fileName}', [DataController::class, 'remove'])->name('remove');




