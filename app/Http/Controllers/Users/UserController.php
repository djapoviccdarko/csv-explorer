<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display the index page with a list of all users.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        // Try to retrieve users from cache
        $users = Cache::remember('users', now()->addMinutes(1), function () {
            return User::paginate(10);
        });

        return view('users.index', compact('users'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {
        // Remove the users cache to refresh the list
        Cache::forget('users');

        // Delete the user from the database
        $user->delete();

        // Redirect back to the previous page with a success message
        return redirect()->back()
            ->withSuccess('The User with name ' . $user->name . ' has been deleted.');
    }

    public function edit(Request $request)
    {
        return view('users.edit')->with([
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     *
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function update(UserRequest $request): RedirectResponse
    {
        // Get the authenticated user
        $user = Auth::user();

        // Update the user's profile information
        $user->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'title' => $request->title,
            'position' => $request->position,
        ]);

        // Save the updated user information
        $user->save();

        // Redirect back to the edit profile page with a success message
        return redirect()
            ->route('edit')
            ->withSuccess('Profile edited.');
    }
}
