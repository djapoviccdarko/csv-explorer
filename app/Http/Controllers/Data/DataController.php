<?php

namespace App\Http\Controllers\Data;

use App\Helpers\CsvDataProcessor;
use App\Http\Controllers\Controller;
use App\Models\Data;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Apply the 'auth' and 'verified' middleware to the controller methods
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a paginated list of CSV data records.
     *
     * @return View
     */
    public function index(): View
    {
        // Remove the data cache to refresh the list
        Cache::forget('data');

        // Try to retrieve data from cache
        $data = Cache::remember('data', now()->addMinutes(1), function () {
            return Data::paginate(10);
        });

        return view('data.index', compact('data'));
    }

    /**
     * Display the details of a specific CSV data record.
     *
     * @param int $id The ID of the CSV data record to show details for.
     * @return View
     */
    public function showDetails(int $id): View
    {
        // Check if the CSV data record is already cached
        if (Cache::has('csv_data_' . $id)) {
            // Retrieve the CSV data record from the cache
            $csvData = Cache::get('csv_data_' . $id);
        } else {
            // Retrieve the CSV data record based on the given ID or throw a 404 exception if not found
            $csvData = Data::findOrFail($id);

            // Cache the CSV data record for 60 seconds
            Cache::put('csv_data_' . $id, $csvData, 60);
        }

        // Return the 'data.details' view and pass the 'csvData' variable to it
        return view('data.details', compact('csvData'));
    }

    /**
     * Display the view for inserting new CSV data records.
     *
     * @return View
     */
    public function insertView(): View
    {
        return view('data.insert');
    }

    /**
     * Process and store the uploaded CSV or text file.
     *
     * @param Request $request The incoming HTTP request containing the file to be uploaded.
     *
     * @return RedirectResponse
     */
    public function upload(Request $request): RedirectResponse
    {
        // Check if any files were uploaded
        if (!$request->hasFile('csv_files')) {
            return redirect()->route('insert')
                ->withErrors(['csv_files' => 'You need to choose at least one file!'])
                ->withInput();
        }

        // Validate the uploaded files
        $request->validate([
            'csv_files.*' => 'required|mimes:csv,txt',
        ]);

        // Get the uploaded files from the request
        $uploadedFiles = $request->file('csv_files');

        $processedFiles = [];
        $errorFiles = [];

        foreach ($uploadedFiles as $uploadedFile) {
            // Check if the file already exists in the 'DESADVdata' folder or 'Data' table
            if (Storage::exists('DESADVdata/' . $uploadedFile->getClientOriginalName())
                || Data::where('file_name', $uploadedFile->getClientOriginalName())->exists()) {
                $errorFiles[] = 'The file <b>' . $uploadedFile->getClientOriginalName() .
                    '</b> already exists in the directory or database.<br>';
                continue;
            }

            // Store the file in the 'DESADVdata' folder with its original name
            $filePath = $uploadedFile->storeAs(
                'DESADVdata',
                $uploadedFile->getClientOriginalName(),
                'local'
            );

            // Validate the structure of the CSV file using CsvDataProcessor::isCsvStructureValid method
            if (!CsvDataProcessor::isCsvStructureValid($filePath)) {
                // Delete the invalid file from storage
                Storage::delete($filePath);
                $errorFiles[] = 'The file <b>' .
                    $uploadedFile->getClientOriginalName() .
                    '</b> structure does not match the expected structure.<br>';
                continue;
            }

            // Process the CSV file (You may need to implement the 'processCsvFile' method)
            CsvDataProcessor::processCsvFile();

            $processedFiles[] = 'The file <b>' .
                $uploadedFile->getClientOriginalName() .
                '</b> has been processed successfully and the data has been entered into the database.<br>';
        }

        $errorMessages = implode(' ', $errorFiles);
        $successMessages = implode(' ', $processedFiles);

        return redirect()->route('insert')
            ->withErrors(['csv_files' => $errorMessages])
            ->with('success', $successMessages);
    }


    /**
     * Remove a file from the 'DESADVdata' directory and its corresponding entry from the database.
     *
     * @param string $fileName The name of the file to be removed.
     * @return RedirectResponse Redirects back to the previous page with success or error message.
     */
    public function remove(string $fileName): RedirectResponse
    {
        // Find the file entry in the database
        $fileEntry = Data::where('file_name', $fileName)->first();

        if ($fileEntry) {
            // Delete the file from the 'DESADVdata' directory
            Storage::delete('DESADVdata/' . $fileName);

            // Delete the file entry from the database
            $fileEntry->delete();

            return redirect()->route('data')->with('success', 'File removed successfully.');
        } else {
            return redirect()->back()->with('error', 'File not found in the database.');
        }
    }

    /**
     * Display search results with pagination.
     *
     * @return View
     */
    public function search(): View
    {
        // Get the search query from the request parameter
        $searchText = $_GET['query'];

        // Perform search using the 'Data' model
        $data = Data::where('file_name', 'LIKE', '%' . $searchText . '%')
            ->orWhere('material', 'LIKE', '%' . $searchText . '%')
            ->orWhere('mfr', 'LIKE', '%' . $searchText . '%')
            ->orWhere('ponr_number', 'LIKE', '%' . $searchText . '%')
            ->orWhere('line_number', 'LIKE', '%' . $searchText . '%')
            ->orWhere('invoice_nr', 'LIKE', '%' . $searchText . '%')
            ->paginate(10);

        return view('data.search', compact('data'));
    }
}
