<?php

namespace App\Console\Commands;

use App\Helpers\CsvDataProcessor;
use Illuminate\Console\Command;

class ProcessCsvFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process CSV files in the DESADVdata directory';

    /**
     * Process CSV files and store data in the database.
     *
     * This method reads CSV files from the 'DESADVdata' storage directory,
     * processes each record, and stores the data in the 'Data' table if it's not already processed.
     */
    public function handle(): void
    {
        CsvDataProcessor::processCsvFile();
        $this->info('CSV files processed successfully.');
    }

}
