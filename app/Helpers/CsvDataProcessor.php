<?php

namespace App\Helpers;

use App\Models\Data;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class CsvDataProcessor
{
    /**
     * Get the mapping of van IDs to their corresponding codes.
     *
     * @return array
     */
    public static function getVanIdMapping(): array
    {
        // The mapping of van IDs to their corresponding suppliers
        return [
            '138128942' => 'SPA',
            'STMEUR' => 'STM',
            'STMZ' => 'STM',
            'OSRAMOSRBG' => 'OSR',
            'OSRAMOSRBGTEST' => 'OSR',
            'SCGPROD' => 'ONS',
            'SCGTEST' => 'ONS',
            'NATLSEMI' => 'NSC',
            '52290567S081' => 'FME',
            '52290567S080' => 'FME',
            'FAIRCHILD' => 'FAI',
            '001325463EMEA' => 'FSL',
            'ATMELBOX1' => 'ATM',
            'VISHAY-EU' => 'VIS',
            'VISHAY-EU-TEST' => 'VIS',
            'DEKTOSHC.DETOEL01' => 'TOS',
            'DEKTOSHC.DETOEL03' => 'TOS',
            'IFX.B2B.PROD' => 'INF',
            'IFX.B2B.TEST' => 'INF',
            'HRS0004' => 'INC',
            'BSI' => 'BSI',
            '5013546104076' => 'NXP',
            '608208245' => 'ISS',
            '084963177GSPGN' => 'AVA',
            'AVAGO-MA' => 'AVA',
            '084963177GSPGNT' => 'AVA',
            'NUMONYXEMEA' => 'NUM',
            'SNWEUR' => 'STE',
            'KAMINO' => '14',
            'SAMSUNGSEMNT' => 'SAM',
            'GBTXI.TXI002US' => 'TID',
            '093120871' => 'MIC',
            '093120871T' => 'MIC',
            '4399902104579' => 'EVL',
            '102108446WP:01' => 'CYP',
            '102108446WP' => 'CYP',
            'IDTEDIUS' => 'IDT',
            'IDTB2BPRD' => 'IDT',
            'IDTB2BTST' => 'IDT',
            'IDTEDITST' => 'IDT',
            'MCHPPROD' => 'MCH',
            'MCHPTEST' => 'MCH',
            'MAXIMACTG' => 'MXM',
            'WEENVANP' => 'WEN',
            'WEENVANT' => 'WEN',
            '491683766VANP' => 'AMP',
            '491683766VANT' => 'AMP',
            '492020517VANP' => 'NEX',
            '492020517VANT' => 'NEX',
            '492020517AS2P' => 'NEX',
            '5013546104069' => 'NXP',
            '9255838400' => 'BRI',
            '9255838400T' => 'BRI',
            '9494501080' => 'BRO',
            'ALTERAINT' => 'ALT',
            'ALTERAIRL' => 'ALT',
            'ANALOGDEVICES' => 'ADI',
            'ANALOGASAP' => 'ADI',
            'MAXIMACTGTEST' => 'MXM',
            'MAXIMACTGMXM' => 'MXM',
        ];
    }

    /**
     * Calculate and return the arrival date based on the despatch date and existing Arrival date.
     *
     * @param Carbon|null $despatchDate The despatch date.
     * @param Carbon|null $existingArrivalDate The existing arrival date.
     * @return Carbon|string|null The calculated arrival date as a Carbon instance or a formatted string, or null.
     */
    public static function calculateArrivalDate(?Carbon $despatchDate, ?Carbon $existingArrivalDate): Carbon|string|null
    {

        if (!empty($existingArrivalDate)) {
            $arrivalDate = $existingArrivalDate->format('d.m.Y'); // Use the record date as the arrival date

        } else {
            // Add 7 days to despatch date to calculate the arrival date
            $arrivalDate = $despatchDate->copy()->addDays(7);
            $arrivalDate = $arrivalDate->format('d.m.Y') . ' (Calculated)';
        }

        return $arrivalDate;
    }

    /**
     * Convert a date string to a Carbon instance.
     *
     * @param string|null $dateString The date string to convert or null if it's empty.
     * @return bool|Carbon|null The converted Carbon instance or null if dateString is null or empty.
     */
    public static function convertDate(?string $dateString): bool|Carbon|null
    {
        return empty($dateString) ? null : Carbon::createFromFormat('d.m.Y', $dateString);
    }

    /**
     * Separate and extract the PONR number and line number from a given record.
     *
     * @param string $record The input record containing the PONR and line numbers.
     * @return array An associative array containing the extracted PONR number and line number.
     */
    public static function separatePonrAndLineNumbers(string $existingPonrAndLineNumber): array
    {
        $ponr_number = null;
        $line_number = null;

        if (!empty($existingPonrAndLineNumber)) {
            $ponr_line = explode(":", $existingPonrAndLineNumber); // Split the record using ':'.

            if (count($ponr_line) === 2) { // Check if there are exactly two parts after splitting.
                $ponr_number = $ponr_line[0]; // Assign the first part to PONR number.
                $line_number = $ponr_line[1]; // Assign the second part to line number.
            }
        }

        return [
            'ponr_number' => $ponr_number,
            'line_number' => $line_number
        ];
    }

    /**
     * Create a new entries in the 'Data' table if it doesn't exist.
     *
     * @param string $filename The name of the CSV file.
     * @param array $record The CSV record data as an array.
     * @return void
     */
    public static function insertData(string $filename, array $record): void
    {
        $vanIdMapping = self::getVanIdMapping();
        if (isset($vanIdMapping[$record[17]])) {
            $supplier = $vanIdMapping[$record[17]];
        }
        $despatchDate = self::convertDate($record[10]);
        $arrivalDate = self::calculateArrivalDate($despatchDate, self::convertDate($record[11]));
        $ponr_line = self::separatePonrAndLineNumbers($record[7]);

        // Check if the entry already exists in the database
        $existingEntry = Data::where('file_name', $filename)->first();

        if (!$existingEntry) {
            Data::create([
                'file_name' => $filename,
                'material' => $record[0],
                'quantity' => $record[1],
                'delivery_id' => $record[2],
                'document_id' => $record[3],
                'tracking_number' => $record[4],
                'hwb' => $record[5],
                'contract_number' => $record[6],
                'ponr_number' => $ponr_line['ponr_number'],
                'line_number' => $ponr_line['line_number'],
                'sonr' => $record[8],
                'invoice_nr' => $record[9],
                'despatch_date' => $despatchDate ? $despatchDate->format('d.m.Y') : null,
                'arrival_date' => $arrivalDate,
                'inco_term' => $record[12],
                'da_packages' => $record[13],
                'pack_list_nr' => $record[14],
                'transport_mode' => $record[15],
                'carrier' => $record[16],
                'van_id' => $record[17],
                'mfr' => $supplier ?? null,
                'interchange_control_reference' => $record[18],
                'message_date' => $record[19],
                'lig1' => $record[20],
                'ctr1' => $record[21],
                'fv1' => $record[22],
                'lig2' => $record[23],
                'ctr2' => $record[24],
                'fv2' => $record[25],
                'lig3' => $record[26],
                'ctr3' => $record[27],
                'fv3' => $record[28],
                'supplier_material' => $record[29],
                'weight' => $record[30],
                'key' => $record[31],
                'line' => $record[32],
            ]);
        }
    }

    /**
     * Process CSV files in the 'DESADVdata' directory.
     * Reads each CSV file, extracts records, and inserts data into the 'Data' table.
     *
     * @return void
     */
    public static function processCsvFile(): void
    {
        // Get a list of CSV files in the 'DESADVdata' directory
        $csvFiles = Storage::files('DESADVdata');

        foreach ($csvFiles as $file) {
            $filename = basename($file); // Get the filename without the path
            $handle = fopen(storage_path('app/' . $file), 'r'); // Open the CSV file for reading

            // Iterate through each record in the CSV file
            while (($record = fgetcsv($handle, 0, '|')) !== false) {
                self::insertData($filename, $record); // Insert data into the 'Data' table
            }

            fclose($handle); // Close the CSV file after reading
        }
    }

    /**
     * Check if the CSV file structure is valid.
     *
     * @param string $filePath The path to the CSV file.
     * @return bool True if the structure is valid, false otherwise.
     */
    public static function isCsvStructureValid(string $filePath): bool
    {
        $expectedStructure = [
            'material',
            'quantity',
            'delivery_id',
            'document_id',
            'tracking_number',
            'hwb',
            'contract_number',
            'ponr_line',
            'sonr',
            'invoice_nr',
            'despatch_date',
            'arrival_date',
            'inco_term',
            'da_packages',
            'pack_list_nr',
            'transport_mode',
            'carrier',
            'van_id',
            'interchange_control_reference',
            'message_date',
            'lig1',
            'ctr1',
            'fv1',
            'lig2',
            'ctr2',
            'fv2',
            'lig3',
            'ctr3',
            'fv3',
            'supplier_material',
            'weight',
            'key',
            'line',
        ];
        // Read the content of the CSV file and store it in a variable
        $csvFileContent = file_get_contents(storage_path('app/' . $filePath));

        // Explode the content into an array of lines using newline as the delimiter
        $csvLines = explode("\n", $csvFileContent);

        // Extract the header row from the first line using the '|' character as the field delimiter
        $headerRow = str_getcsv($csvLines[0], '|');

        return count($headerRow) === count($expectedStructure);
    }
}
