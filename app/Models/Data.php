<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'file_name',
        'material',
        'quantity',
        'delivery_id',
        'document_id',
        'tracking_number',
        'hwb',
        'contract_number',
        'ponr_number',
        'line_number',
        'sonr',
        'invoice_nr',
        'despatch_date',
        'arrival_date',
        'inco_term',
        'da_packages',
        'pack_list_nr',
        'transport_mode',
        'carrier',
        'van_id',
        'mfr',
        'interchange_control_reference',
        'message_date',
        'lig1',
        'ctr1',
        'fv1',
        'lig2',
        'ctr2',
        'fv2',
        'lig3',
        'ctr3',
        'fv3',
        'supplier_material',
        'weight',
        'key',
        'line',
    ];
}
